//
//  HomePresenter.swift
//  viper
//
//  Created by Macbook on 9/5/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import Foundation

class HomePresenter  {
    
    var stringCollection = [String]()
    
    // MARK: Properties
    weak var view: HomeViewProtocol?
    var interactor: HomeInteractorInputProtocol?
    var wireFrame: HomeWireFrameProtocol?
    
}

extension HomePresenter: HomePresenterProtocol {
    func getDataNextViewDetalle(object: movieModel.Results) {
        wireFrame?.presentNextViewDetalle(from: view!, object: object)
    }
    
    // TODO: implement presenter methods
    func viewDidLoad() {
        //Se consulta al interactor para extraer los datos remotos
        interactor?.getExternalData()
        view?.iniciar()
    }
    
    
    func getDataNextView(listPost: [postModel.Results], getListDetail: [detailPostModel.Results]){
        wireFrame?.presentNextView(from: view!, listPost: listPost, getListDetail: getListDetail)
    }
}

extension HomePresenter: HomeInteractorOutputProtocol {
    // TODO: implement interactor output methods
    func callBackData(listPost: [postModel.Results], getListDetail: [detailPostModel.Results]) {
        view?.callBackData(listPost: listPost, getListDetail: getListDetail)
    }
}
