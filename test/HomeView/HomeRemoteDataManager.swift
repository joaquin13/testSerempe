//
//  HomeRemoteDataManager.swift
//  viper
//
//  Created by Macbook on 9/5/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import Foundation
import Alamofire

class HomeRemoteDataManager:HomeRemoteDataManagerInputProtocol {
    
    var remoteRequestHandler: HomeRemoteDataManagerOutputProtocol?
    var getListRated = [movieModel.Results]()
    var getListRecomendaciones = [movieModel.Results]()
    var getListFavoritas = [movieModel.Results]()
    var getListPost = [postModel.Results]()
    var getListDetail = [detailPostModel.Results]()
    
    func externalGetData() {
        
        
        let URL1 = "https://jsonplaceholder.typicode.com/posts"
        Alamofire.request(URL1).responseJSON { response in
              if let JSON = response.result.value{
                 let jsonArray = JSON as? NSArray
                 for item in jsonArray! as! [NSDictionary]{
                     let userId = item["userId"] as? Int ?? 0
                     let id = item["id"] as? Int ?? 0
                     let title = item["title"] as? String ?? ""
                     let body = item["body"] as? String ?? ""
                     let objPost = postModel.Results(userId : userId,
                                                     id : id,
                                                     title : title,
                                                     body : body)
                     self.getListPost.append(objPost)
                 }
                  let URL2 = "https://jsonplaceholder.typicode.com/users"
                  Alamofire.request(URL2).responseJSON { response in
                        if let JSON = response.result.value{
                           let jsonArray = JSON as? NSArray
                           for item in jsonArray! as! [NSDictionary]{
                               let _ = item["email"] as? String ?? ""
                               let dicAddress = item["address"] as! NSDictionary
                               let dicCompany = item["company"] as! NSDictionary
                               let dicGeo = dicAddress["geo"] as! NSDictionary
                               
                               let objGeo = detailPostModel.GeoAddress(lat : dicGeo["lat"] as? String ?? "",
                                                                       lng : dicGeo["lng"] as? String ?? "")
                               
                               let objAddress = detailPostModel.Address(street : dicAddress["street"] as? String ?? "",
                                                                        suite : dicAddress["suite"] as? String ?? "",
                                                                        city : dicAddress["city"] as? String ?? "",
                                                                        zipcode : dicAddress["zipcode"] as? String ?? "",
                                                                        geo : objGeo)
                               
                               let objCompany = detailPostModel.Company(name : dicCompany["name"] as? String ?? "",
                                                                        catchPhrase : dicCompany["catchPhrase"] as? String ?? "",
                                                                        bs : dicCompany["bs"] as? String ?? "")
                               
                               let objDetail = detailPostModel.Results(id: item["id"] as? Int ?? 0,
                                                                       name: item["name"] as? String ?? "",
                                                                       username: item["username"] as? String ?? "",
                                                                       email : item["email"] as? String ?? "",
                                                                       address: objAddress,
                                                                       phone : item["phone"] as? String ?? "",
                                                                       webSide : item["website"] as? String ?? "",
                                                                       company : objCompany)
                               self.getListDetail.append(objDetail)
                           }
                            self.remoteRequestHandler?.callBackData(listPost: self.getListPost,
                                                                    getListDetail: self.getListDetail)
                        }
                  }
              }
        }
    }
    
}
