//
//  movieModel.swift
//  test
//
//  Created by Macbook on 11/22/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

public class movieModel {
    
    class servicio: Mappable {
        var results:[Results]?
        
        required init?(map: Map){
            
        }
        
        func mapping(map: Map) {
            results <- map["results"]
            results <- map["items"]
        }
    }
    
    class Results: Mappable {
        var popularity: Double = 0.0
        var vote_count: Double = 0.0
        var poster_path: String = ""
        var original_language: String = ""
        var original_title: String = ""
        var title: String = ""
        var vote_average: Double = 0.0
        var overview: String = ""
        var release_date: String = ""
        
        init(popularity : Double,
             vote_count : Double,
             poster_path : String,
             original_language : String,
             original_title : String,
             title : String,
             vote_average : Double,
             overview : String,
             release_date : String) {
            self.popularity = popularity
            self.vote_count = vote_count
            self.poster_path = poster_path
            self.original_language = original_language
            self.original_title = original_title
            self.title = title
            self.vote_average = vote_average
            self.overview = overview
            self.release_date = release_date
        }
        
        required init?(map: Map){
        }
        
        func mapping(map: Map) {
            popularity <- map["popularity"]
            vote_count <- map["vote_count"]
            poster_path <- map["poster_path"]
            original_language <- map["original_language"]
            original_title <- map["original_title"]
            title <- map["title"]
            vote_average <- map["vote_average"]
            overview <- map["overview"]
            release_date <- map["release_date"]
        }
    }
    
}


public class postModel {
    
    class Results{
        var userId: Int = 0
        var id: Int = 0
        var title: String = ""
        var body: String = ""
        var isFavorite: Bool = false
        var paintBlue: Bool = true
        
        init(userId : Int,
             id : Int,
             title : String,
             body : String) {
            self.userId = userId
            self.id = id
            self.title = title
            self.body = body
        }
    }
}


public class detailPostModel {
    
    class Results{
        var id: Int = 0
        var name: String = ""
        var username: String = ""
        var email: String = ""
        var address: Address?
        var phone : String = ""
        var webSide : String = ""
        var company : Company?
        
        init(id: Int,
             name: String,
             username: String,
             email : String,
             address: Address,
             phone : String,
             webSide : String,
             company : Company) {
            self.id = id
            self.name = name
            self.username = username
            self.email = email
            self.address = address
            self.phone = phone
            self.webSide = webSide
            self.company = company
        }
    }
    
    class Address{
        var street : String = ""
        var suite : String = ""
        var city : String = ""
        var zipcode : String = ""
        var geo : GeoAddress?
        
        init(street : String,
             suite : String,
             city : String,
             zipcode : String,
             geo : GeoAddress) {
            self.street = street
            self.suite = suite
            self.city = city
            self.zipcode = zipcode
            self.geo = geo
            
        }
    }
    
    class GeoAddress{
        var lat : String = ""
        var lng : String = ""
        
        init(lat : String,
             lng : String) {
            self.lat = lat
            self.lng = lng
        }
    }
    
    class Company{
        var name : String = ""
        var catchPhrase : String = ""
        var bs : String = ""
        
        init(name : String,
             catchPhrase : String,
             bs : String) {
            self.name = name
            self.catchPhrase = catchPhrase
            self.bs = bs
        }
    }
}

class FavoritesPost : Object{
    @objc dynamic var idPost = 0
}


