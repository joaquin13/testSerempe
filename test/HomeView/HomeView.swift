//
//  HomeView.swift
//  test
//
//  Created by Macbook on 11/21/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class HomeView: UIViewController {
    
    
    @IBOutlet weak var loadIndicator: UIActivityIndicatorView!
    var getListPost = [postModel.Results]()
    
    // MARK: Properties
    var presenter: HomePresenterProtocol?
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        
        
    }
    
    
    
    
}

extension HomeView: HomeViewProtocol {
    // TODO: implement view output methods
    func callBackData(listPost: [postModel.Results], getListDetail: [detailPostModel.Results]) {
        self.getListPost.removeAll()
        self.getListPost = listPost
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            self.loadIndicator.stopAnimating()
            self.loadIndicator.hidesWhenStopped = true
            self.presenter?.getDataNextView(listPost: listPost, getListDetail: getListDetail)
        }
    }
    
    func iniciar() {
        DispatchQueue.main.async {
            self.loadIndicator.startAnimating()
        }
    }
    
    func detener() {
    }
}

