//
//  ProtocolEventTable.swift
//  test
//
//  Created by Joaquin Martinez on 11/11/21.
//  Copyright © 2021 Macbook. All rights reserved.
//

import Foundation

protocol TableNavigation {
    func genericAction(post : postModel.Results)
}
