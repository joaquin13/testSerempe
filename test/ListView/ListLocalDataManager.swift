//
//  ListLocalDataManager.swift
//  test
//
//  Created by Macbook on 11/22/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import Foundation
import RealmSwift

class ListLocalDataManager:ListLocalDataManagerInputProtocol {
    var localRequestHandler: ListLocalDataManagerOutputProtocol?
    
    
    func getLocalDataFavoritesPost(){
        var listFavorites = [FavoritesPost]()
        let realm = try! Realm()
        let items = realm.objects(FavoritesPost.self)
        for item in items{
            let favorite  = FavoritesPost()
            favorite.idPost = item.idPost
            listFavorites.append(favorite)
        }
        self.localRequestHandler?.callBackData(listFavorites: listFavorites )
    }
    
    func saveOrDeleteFavoritePost(flag : Bool, model: postModel.Results){
        let realm = try! Realm()
        if flag {
            let item = FavoritesPost()
            item.idPost = model.id
            try! realm.write{
                realm.add(item)
            }
        }else{
            let item = FavoritesPost()
            item.idPost = model.id
            try! realm.write {
                realm.delete(realm.objects(FavoritesPost.self).filter("idPost = \(model.id)"))
            }
        }
    }
}
