//
//  ListPresenter.swift
//  test
//
//  Created by Macbook on 11/22/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import Foundation

class ListPresenter  {
    
    // MARK: Properties
    weak var view: ListViewProtocol?
    var interactor: ListInteractorInputProtocol?
    var wireFrame: ListWireFrameProtocol?
    var objectsMovie: [movieModel.Results]?
    var listPost : [postModel.Results]?
    var listDetail: [detailPostModel.Results]?
}

extension ListPresenter: ListPresenterProtocol {
    func getExternalData() {
        self.interactor?.getExternalData()
    }
    
    func getDataNextView(data : detailPostModel.Results) {
        wireFrame?.presentNextView(from: view!, object: data)
    }
    
    func saveOrDeleteFavoritePost(flag : Bool, model: postModel.Results){
        self.interactor?.saveOrDeleteFavoritePost(flag: flag, model: model)
    }
    
    // TODO: implement presenter methods
    func viewDidLoad() {
        
        guard let arrayPost = self.listPost else {return}
        self.interactor?.getFavoritesPost(listPost : arrayPost)
        
    }
}

extension ListPresenter: ListInteractorOutputProtocol {
    // TODO: implement interactor output methods
    
    func callBackData(listPost : [postModel.Results], listPostFavorite : [postModel.Results]){
        guard let arrayDetail = self.listDetail else {return}
        self.view?.showMovies(listPost : listPost,listPostFavorites: listPostFavorite, listDetail: arrayDetail)
    }
    
    func callBackData(listPost: [postModel.Results], getListDetail: [detailPostModel.Results]){
        self.listPost = listPost
        self.listDetail = getListDetail
        guard let arrayPost = self.listPost else {return}
        self.interactor?.getFavoritesPost(listPost : arrayPost)
    }
}
