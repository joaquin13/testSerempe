//
//  ListProtocols.swift
//  test
//
//  Created by Macbook on 11/22/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import Foundation
import UIKit

protocol ListViewProtocol: AnyObject {
    // PRESENTER -> VIEW
    var presenter: ListPresenterProtocol? { get set }
    func showMovies(listPost : [postModel.Results],listPostFavorites: [postModel.Results], listDetail: [detailPostModel.Results])
    
}

protocol ListWireFrameProtocol: AnyObject {
    // PRESENTER -> WIREFRAME
    static func createListModule(listPost: [postModel.Results], getListDetail: [detailPostModel.Results]) -> UIViewController
    func presentNextView(from view : ListViewProtocol, object: detailPostModel.Results)
}

protocol ListPresenterProtocol: AnyObject {
    // VIEW -> PRESENTER
    var view: ListViewProtocol? { get set }
    var interactor: ListInteractorInputProtocol? { get set }
    var wireFrame: ListWireFrameProtocol? { get set }
    var objectsMovie: [movieModel.Results]? { get set }
    var listPost : [postModel.Results]? { get set }
    var listDetail: [detailPostModel.Results]? { get set }
    func viewDidLoad()
    func saveOrDeleteFavoritePost(flag : Bool, model: postModel.Results)
    func getDataNextView(data : detailPostModel.Results)
    func getExternalData()
}

protocol ListInteractorOutputProtocol: AnyObject {
// INTERACTOR -> PRESENTER
    func callBackData(listPost : [postModel.Results], listPostFavorite : [postModel.Results])
    func callBackData(listPost: [postModel.Results], getListDetail: [detailPostModel.Results])
}

protocol ListInteractorInputProtocol: AnyObject {
    // PRESENTER -> INTERACTOR
    var presenter: ListInteractorOutputProtocol? { get set }
    var localDatamanager: ListLocalDataManagerInputProtocol? { get set }
    var remoteDatamanager: ListRemoteDataManagerInputProtocol? { get set }
    
    func saveOrDeleteFavoritePost(flag : Bool, model: postModel.Results)
    func getFavoritesPost(listPost : [postModel.Results])
    func getExternalData()
}

protocol ListDataManagerInputProtocol: AnyObject {
    // INTERACTOR -> DATAMANAGER
}

protocol ListRemoteDataManagerInputProtocol: AnyObject {
    // INTERACTOR -> REMOTEDATAMANAGER
    var remoteRequestHandler: ListRemoteDataManagerOutputProtocol? { get set }
    func getExternalData()
}

protocol ListRemoteDataManagerOutputProtocol: AnyObject {
    // REMOTEDATAMANAGER -> INTERACTOR
    func callBackData(listPost: [postModel.Results], getListDetail: [detailPostModel.Results] )
}

protocol ListLocalDataManagerInputProtocol: AnyObject {
    // INTERACTOR -> LOCALDATAMANAGER
    var localRequestHandler: ListLocalDataManagerOutputProtocol? { get set }
    func getLocalDataFavoritesPost()
    func saveOrDeleteFavoritePost(flag : Bool, model: postModel.Results)
}

protocol ListLocalDataManagerOutputProtocol: AnyObject {
    // LOCALDATAMANAGER -> INTERACTOR
    func callBackData(listFavorites: [FavoritesPost] )
}
