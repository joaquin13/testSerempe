//
//  ListView.swift
//  test
//
//  Created by Macbook on 11/22/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import Foundation
import UIKit

class ListView: UIViewController {
    
    
    @IBOutlet weak var tableMovie: UITableView!
    @IBOutlet var segmentedControl: UISegmentedControl!
    @IBOutlet var btnReload: UIButton!
    @IBOutlet var btnDelete: UIButton!
    var listPost = [postModel.Results]()
    var listFavoritePost = [postModel.Results]()
    var listDetail = [detailPostModel.Results]()
    var isFavoriteFlag = false
    
    
    // MARK: Properties
    var presenter: ListPresenterProtocol?

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.presenter?.viewDidLoad()
    }
    
    @IBAction func selectTipping(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex{
        case 0:
            self.isFavoriteFlag = false
            self.tableMovie.reloadData()
        case 1:
            self.isFavoriteFlag = true
            self.tableMovie.reloadData()
        default:break
        }
    }

    @IBAction func reloadList(_ sender: Any) {
        self.presenter?.getExternalData()
        self.listPost.removeAll()
        self.listDetail.removeAll()
        self.listFavoritePost.removeAll()
        self.tableMovie.reloadData()
    }
    
    @IBAction func deleteList(_ sender: Any) {
        self.listPost.removeAll()
        self.listDetail.removeAll()
        self.listFavoritePost.removeAll()
        self.tableMovie.reloadData()
    }
    
}

extension ListView: ListViewProtocol {
    // TODO: implement view output methods
    func showMovies(listPost : [postModel.Results],listPostFavorites: [postModel.Results], listDetail: [detailPostModel.Results]){
        self.listPost.removeAll()
        self.listDetail.removeAll()
        self.listFavoritePost.removeAll()
        self.listPost = listPost
        self.listDetail = listDetail
        self.listFavoritePost = listPostFavorites
        DispatchQueue.main.async {
            self.tableMovie.reloadData()
        }
    }
}

extension ListView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            if self.isFavoriteFlag{
                let obj = self.listFavoritePost.remove(at: indexPath.row)
                if let idx = self.listPost.firstIndex(where: { $0.id == obj.id }) {
                    self.listPost[idx].isFavorite = false
                }
                self.tableMovie.reloadData()
                self.presenter?.saveOrDeleteFavoritePost(flag : false, model: obj)
            }else{
                let obj = self.listPost.remove(at: indexPath.row)
                if let idx = self.listFavoritePost.firstIndex(where: { $0.id == obj.id }) {
                    self.listFavoritePost.remove(at: idx)
                    self.presenter?.saveOrDeleteFavoritePost(flag : false, model: obj)
                }
                self.tableMovie.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isFavoriteFlag{
            return self.listFavoritePost.count
        }else{
            return self.listPost.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemPostCellController") as!  itemPostCellController
        if self.isFavoriteFlag{
            cell.drawCell(post: self.listFavoritePost[indexPath.row])
        }else{
            if indexPath.row < 20 && self.listPost[indexPath.row].paintBlue {
                self.listPost[indexPath.row].paintBlue = true
            }else{
                self.listPost[indexPath.row].paintBlue = false
            }
            cell.drawCell(post: self.listPost[indexPath.row])
        }
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    
}

extension ListView: UITableViewDelegate {
    // TODO: implement view output methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isFavoriteFlag{
            let userId = self.listFavoritePost[indexPath.row].userId
            if let objectDetail = self.listDetail.first(where: {$0.id == userId}){
                presenter?.getDataNextView(data : objectDetail)
            }
        }else{
            let userId = self.listPost[indexPath.row].userId
            if self.listPost[indexPath.row].paintBlue{
                self.listPost[indexPath.row].paintBlue = false
            }
            if let objectDetail = self.listDetail.first(where: {$0.id == userId}){
                presenter?.getDataNextView(data : objectDetail)
            }
        }
    }
}


extension ListView{
    func prepareUI(){
        let nameCell = "itemPostCellController"
        let nib = UINib(nibName: nameCell, bundle: nil)
        self.tableMovie.register(nib, forCellReuseIdentifier: nameCell)
        self.floatingButton(button: self.btnDelete, title : "Delete All")
        self.floatingButton(button: self.btnReload, title : "Reload")
    }
    
    func floatingButton(button : UIButton, title : String){
        button.setTitle(title, for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        button.clipsToBounds = true
        button.layer.cornerRadius = 20
        button.setTitleColor(.white, for: .normal)
    }
    
}

extension ListView : TableNavigation{
    func genericAction(post : postModel.Results) {
        if self.isFavoriteFlag{
            if !post.isFavorite{
                if let idx = self.listFavoritePost.firstIndex(where: { $0.id == post.id }) {
                    self.listFavoritePost.remove(at: idx)
                }
                if let idx = self.listPost.firstIndex(where: { $0.id == post.id }) {
                    self.listPost[idx].isFavorite = false
                }
                self.tableMovie.reloadData()
            }
        }else{
            if post.isFavorite{
                if let idx = self.listPost.firstIndex(where: { $0.id == post.id }) {
                    self.listPost[idx].isFavorite = true
                }
                self.listFavoritePost.append(post)
            }else{
                if let idx = self.listPost.firstIndex(where: { $0.id == post.id }) {
                    self.listPost[idx].isFavorite = false
                }
                if let idx = self.listFavoritePost.firstIndex(where: { $0.id == post.id }) {
                    self.listFavoritePost.remove(at: idx)
                }
            }
            self.tableMovie.reloadData()
        }
        self.presenter?.saveOrDeleteFavoritePost(flag : post.isFavorite, model: post)
    }
}
