//
//  ListInteractor.swift
//  test
//
//  Created by Macbook on 11/22/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import Foundation

class ListInteractor: ListInteractorInputProtocol {
    
    

    // MARK: Properties
    weak var presenter: ListInteractorOutputProtocol?
    var localDatamanager: ListLocalDataManagerInputProtocol?
    var remoteDatamanager: ListRemoteDataManagerInputProtocol?
    var listPost : [postModel.Results]?
    var listPostFavorite = [postModel.Results]()
    
    
    func saveOrDeleteFavoritePost(flag : Bool, model: postModel.Results){
        self.localDatamanager?.saveOrDeleteFavoritePost(flag: flag, model: model)
    }
    
    func getFavoritesPost(listPost : [postModel.Results]){
        self.listPost = listPost
        self.localDatamanager?.getLocalDataFavoritesPost()
    }
    
    func getExternalData() {
        self.remoteDatamanager?.getExternalData()
    }
    
}

extension ListInteractor: ListRemoteDataManagerOutputProtocol {
    // TODO: Implement use case methods
    func callBackData(listPost: [postModel.Results], getListDetail: [detailPostModel.Results] ){
        self.presenter?.callBackData(listPost: listPost, getListDetail: getListDetail)
    }
}

extension ListInteractor: ListLocalDataManagerOutputProtocol {
    // TODO: Implement use case methods
    func callBackData(listFavorites: [FavoritesPost]) {
        self.listPostFavorite.removeAll()
        for favorite in listFavorites{
            if let idx = self.listPost!.firstIndex(where: { $0.id == favorite.idPost }) {
                self.listPost![idx].isFavorite = true
                self.listPostFavorite.append(self.listPost![idx])
            }
        }
        self.presenter?.callBackData(listPost : self.listPost!, listPostFavorite : self.listPostFavorite)
    }
}
