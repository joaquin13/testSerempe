//
//  DetalleProtocols.swift
//  test
//
//  Created by Macbook on 11/22/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import Foundation
import UIKit

protocol DetalleViewProtocol: AnyObject {
    // PRESENTER -> VIEW
    var presenter: DetallePresenterProtocol? { get set }
    func showDetalle(data:detailPostModel.Results)
    
}

protocol DetalleWireFrameProtocol: AnyObject {
    // PRESENTER -> WIREFRAME
    static func createDetalleModule(data : detailPostModel.Results) -> UIViewController
}

protocol DetallePresenterProtocol: AnyObject {
    // VIEW -> PRESENTER
    var view: DetalleViewProtocol? { get set }
    var interactor: DetalleInteractorInputProtocol? { get set }
    var wireFrame: DetalleWireFrameProtocol? { get set }
    var datoBeforeView : movieModel.Results? {get set}
    var dataDetail : detailPostModel.Results? {get set}
    
    func viewDidLoad()
}

protocol DetalleInteractorOutputProtocol: AnyObject {
// INTERACTOR -> PRESENTER
}

protocol DetalleInteractorInputProtocol: AnyObject {
    // PRESENTER -> INTERACTOR
    var presenter: DetalleInteractorOutputProtocol? { get set }
    var localDatamanager: DetalleLocalDataManagerInputProtocol? { get set }
    var remoteDatamanager: DetalleRemoteDataManagerInputProtocol? { get set }
}

protocol DetalleDataManagerInputProtocol: AnyObject {
    // INTERACTOR -> DATAMANAGER
}

protocol DetalleRemoteDataManagerInputProtocol: AnyObject {
    // INTERACTOR -> REMOTEDATAMANAGER
    var remoteRequestHandler: DetalleRemoteDataManagerOutputProtocol? { get set }
}

protocol DetalleRemoteDataManagerOutputProtocol: AnyObject {
    // REMOTEDATAMANAGER -> INTERACTOR
}

protocol DetalleLocalDataManagerInputProtocol: AnyObject {
    // INTERACTOR -> LOCALDATAMANAGER
}
