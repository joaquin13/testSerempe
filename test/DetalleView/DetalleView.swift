//
//  DetalleView.swift
//  test
//
//  Created by Macbook on 11/22/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import Foundation
import UIKit

class DetalleView: UIViewController {

    
    @IBOutlet weak var lblId: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblWebSide: UILabel!
    @IBOutlet weak var lblCompany: UILabel!
    // MARK: Properties
    var presenter: DetallePresenterProtocol?

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
}

extension DetalleView: DetalleViewProtocol {
    // TODO: implement view output methods
    func showDetalle(data:detailPostModel.Results){
        
        self.lblId.text = "Id: \(data.id)"
        self.lblName.text = "Name: " + data.name
        self.lblUserName.text = "User Name: " + data.username
        self.lblEmail.text = "Email: " + data.email
        self.lblPhone.text = "Phone: " + data.phone
        self.lblWebSide.text = "WebSide: " + data.webSide
        guard let objAddress = data.address, let objGeo = objAddress.geo, let objCompany = data.company else {return}
        self.lblAddress.text = "Address: " + objAddress.street +
        ", " + objAddress.suite + ", " + objAddress.city + ", " + objAddress.zipcode + ", " + objAddress.zipcode +
        ", lat: " + objGeo.lat + " lng: " + objGeo.lng
        self.lblCompany.text = objCompany.name + ", " + objCompany.catchPhrase + ", " + objCompany.bs
        
    }
}
