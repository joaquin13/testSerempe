//
//  itemPostCellController.swift
//  test
//
//  Created by Joaquin Martinez on 11/11/21.
//  Copyright © 2021 Macbook. All rights reserved.
//

import UIKit

class itemPostCellController: UITableViewCell{

    @IBOutlet weak var userId: UILabel!
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imageFavorite: UIImageView!{
        didSet{
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(actionFavoriteImage))
            imageFavorite.isUserInteractionEnabled = true
            imageFavorite.addGestureRecognizer(tapGesture)
        }
    }
    var post : postModel.Results?
    var delegate : TableNavigation?

    
    func drawCell(post : postModel.Results){
        self.post = post
        self.userId.text = "userId: \(post.userId)"
        self.id.text = "id: \(post.id)"
        self.title.text = "title: " + post.title
        self.body.text = "body: " + post.body
        let origImage = UIImage(named: "starFavorite")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        self.imageFavorite.image = tintedImage
        guard let objPost = self.post else {return}
        if objPost.isFavorite{
            self.imageFavorite.tintColor = .yellow
        }else{
            self.imageFavorite.tintColor = .gray
        }
        
        if objPost.paintBlue {
            self.mainView.backgroundColor = .systemBlue
        }else{
            self.mainView.backgroundColor = .systemBackground
        }
        
    }
    
    @objc func actionFavoriteImage(){
        guard let objPost = self.post else {return}
        objPost.isFavorite = !objPost.isFavorite
        self.delegate?.genericAction(post: objPost)
    }

    
}
